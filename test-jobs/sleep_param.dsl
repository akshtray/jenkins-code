folder('test-area') {
}

job('test-area/sleep-params') {
  parameters {
    stringParam('SLEEP_TIME') 
  }
  steps {
    shell('echo sleeping for $SLEEP_TIME seconds')
    shell('sleep $SLEEP_TIME')
    shell('echo done sleeping')
  }  
}
